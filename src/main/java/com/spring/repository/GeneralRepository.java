package com.spring.repository;

import org.springframework.data.repository.CrudRepository;

import com.spring.entity.General;

public interface GeneralRepository  extends CrudRepository<General,String> {

}
