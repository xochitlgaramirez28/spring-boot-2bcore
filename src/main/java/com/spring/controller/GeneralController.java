package com.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.spring.service.GeneralService;


@Controller
@RequestMapping("/general")
public class GeneralController {
	private static final Logger logger = LoggerFactory.getLogger(GeneralController.class);

	@Autowired
	private GeneralService generalService;
	
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public String listaCountries(ModelMap mp) {
		System.out.println(generalService.findAll().toString());
		logger.debug(generalService.findAll().toString());
		mp.put("generales", generalService.findAll());
		return "general"; 
	}
}
