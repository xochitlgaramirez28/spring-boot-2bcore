package com.spring.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "general")
public class General {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private Long id;
	
	@Column(name="varchar", length=50, nullable = false)
	private String cadena;
	
	@Column(name="int", nullable = false)
	private Integer entero;
	
	@Column(name="real", nullable = false)
	private Integer real;
	
	
	@Column(name="numeric", nullable = false)
	private Integer numeric;

	public General() {
		super();
		// TODO Auto-generated constructor stub
	}


	public General(Long id, String cadena, Integer entero, Integer real, Integer numeric) {
		super();
		this.id = id;
		this.cadena = cadena;
		this.entero = entero;
		this.real = real;
		this.numeric = numeric;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getCadena() {
		return cadena;
	}


	public void setCadena(String cadena) {
		this.cadena = cadena;
	}


	public Integer getEntero() {
		return entero;
	}


	public void setEntero(Integer entero) {
		this.entero = entero;
	}


	public Integer getReal() {
		return real;
	}


	public void setReal(Integer real) {
		this.real = real;
	}


	public Integer getNumeric() {
		return numeric;
	}


	public void setNumeric(Integer numeric) {
		this.numeric = numeric;
	}


	@Override
	public String toString() {
		return "General [id=" + id + ", cadena=" + cadena + ", entero=" + entero + ", real=" + real + ", numeric="
				+ numeric + "]";
	}
	
}
