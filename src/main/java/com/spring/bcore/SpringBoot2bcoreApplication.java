package com.spring.bcore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages= {"com.spring"})
@EnableJpaRepositories("com.spring.repository")
@EntityScan(basePackages= {"com.spring.entity"})
public class SpringBoot2bcoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot2bcoreApplication.class, args);
	}

}
