package com.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.entity.General;
import com.spring.repository.GeneralRepository;

@Service
public class GeneralService {
	
    private final GeneralRepository generalRepository;
    
    @Autowired
    public GeneralService(GeneralRepository generalRepository) {
        this.generalRepository = generalRepository;
    }
 
    public List<General> findAll() {
        return (List<General>) generalRepository.findAll();
    }

}
